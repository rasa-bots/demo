Getting started
=============================================
1. Run `code-server --auth none` in a terminal in studiolab  
2. Fetch the studiolab URL (example, https://xxxxxxxxxxxxxxxxxx.studio.us-east-2.sagemaker.aws/studiolab/default/jupyter/lab)  
3. Change /lab to /proxy/8080/ to construct the VScode URL (example, https://xxxxxxxxxxxxxxxxxx.studio.us-east-2.sagemaker.aws/studiolab/default/jupyter/proxy/8080/)   
4. Use the constructed URL to open up the VScode server in a new tab and wait for 3-5 minutes for the server to come up.   
5. Turn off gpu acceleration for VSCode terminal by changing the VScode setting `terminal.integrated.gpuAcceleration`. This should remove the blurring of text in the terminal.  
6. Activate the rasa-env conda environment by running `conda activate rasa-env`.  
7. Run `rasa train` to generate the model and then run `rasa shell` to interact with the bot.  


Creation of conda environment for Rasa
=============================================
**Conda setup**

conda create --name rasa-env python==3.8  
conda activate rasa-env  

python -m pip uninstall pip  
python -m ensurepip  
python -m pip install -U pip  

python -m pip install --upgrade setuptools  
python -m pip install rasa  

conda deactivate  


**Git setup**

conda install openssh

**Rasa setup**

rasa init  
rasa train   [If changes made]  
rasa shell  

**VScode setup**

conda install -y -c conda-forge code-server  
code-server --auth none  